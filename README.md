# Concourse Labs CLI Scan Tool

## Release Notes

### 6.0.2 (Latest)
- Fixed bug that would cause false positives in some cases.

### 6.0.1
- Added additional model info output on saved model evaluation.
- Updated `--help`.

### 6.0.0
- Added support for updated violation workflow.

### 5.0.2
- Security fixes.

### 5.0.1
- Adjusted IaC template validation.

### 5.0.0
- Added support for creating a Concourse Labs Asset for each file being evaluated, which also allows users to view policy violations in the Concourse Labs application.
    - IaC file name is used for asset name.
    - Owning group id is required.
    - **THIS IS NOW THE DEFAULT BEHAVIOR**
- Added basic validation for Terraform Plan files.
- Updated all environment variables replacing `CONCOURSE_` with `CL_` prefix.
    - Example: `CL_SURFACE_LAYER`
- Required configuration includes (arg or env var):
    - `-u, --username` or `CL_USERNAME`
    - `-s, --secret` or `CL_SECRET`
    - `-sl, --surface-layer` or `CL_SURFACE_LAYER`
    - `-iac` or `CL_IAC_TYPE`
    - `-og, --owning-group` or `CL_OWNING_GROUP`
- `CONCOURSELABS_DNS` was updated to `CL_PLATFORM_ENV`
- Added new `-dr, --dry-run` parameter to perform an unsaved asset evaluation.

### 4.0.0
- Added new environment variable `CONCOURSELABS_DNS` supporting use of ConcourseLabs on other available cloud providers. Default setting is AWS.

### 3.0.0

- Add new **required** CLI argument `-iac` to allow specifying the type of IaC file being scanned.
- Add new corresponding environment variable `CONCOURSE_IAC_TYPE`
- Add new corresponding configuration file parameter `iac`
- Supported IaC types are:
    - `ansible`
    - `aws`
    - `kubernetes`
    - `openshift`
    - `terraform`

